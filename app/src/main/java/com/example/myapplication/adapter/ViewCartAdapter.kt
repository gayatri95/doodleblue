package com.example.myapplication.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.model.Dish
import kotlinx.android.synthetic.main.item_dish.view.*

class ViewCartAdapter : RecyclerView.Adapter<ViewCartAdapter.DishViewHolder>() {

    var distList = ArrayList<Dish>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DishViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_cart_item, parent, false)
        return DishViewHolder(view)

    }

    override fun getItemCount(): Int {
        return distList.size
    }

    fun submitList(list: ArrayList<Dish>) {
        distList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: DishViewHolder, position: Int) {

        val dish = distList[position]

        holder.itemView.tv_dish_name.text = dish.name
        holder.itemView.tv_dish_desc.text = dish.desc
        holder.itemView.tv_dish_price.text =
            String.format(holder.itemView.context.getString(R.string.dish_price), dish.price.toString())
        holder.itemView.tv_count_dish.text = dish.count.toString()

    }

    class DishViewHolder(item: View) : RecyclerView.ViewHolder(item)

}