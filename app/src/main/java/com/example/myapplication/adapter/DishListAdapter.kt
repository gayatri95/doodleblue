package com.example.myapplication.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.model.Dish
import kotlinx.android.synthetic.main.item_dish.view.*

class DishListAdapter : RecyclerView.Adapter<DishListAdapter.DishViewHolder>() {

    var distList = ArrayList<Dish>()
    private var dishClickListener: OnDishClickListener? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DishViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_dish, parent, false)
        return DishViewHolder(view)

    }

    override fun getItemCount(): Int {
        return distList.size
    }

    fun submitList(list: ArrayList<Dish>) {
        distList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: DishViewHolder, position: Int) {

        val dish = distList[position]

        holder.itemView.tv_dish_name.text = dish.name
        holder.itemView.tv_dish_desc.text = dish.desc
        holder.itemView.tv_dish_price.text =
            String.format(holder.itemView.context.getString(R.string.dish_price), dish.price.toString())
        holder.itemView.tv_count_dish.text = dish.count.toString()


        if (dish.count <= 0) {
            holder.itemView.btn_add?.visibility = View.VISIBLE
            holder.itemView.grp_add_minus_view?.visibility = View.INVISIBLE
        } else {
            holder.itemView.btn_add?.visibility = View.GONE
            holder.itemView.grp_add_minus_view?.visibility = View.VISIBLE
        }

        holder.itemView.btn_add.setOnClickListener {
            if (position != RecyclerView.NO_POSITION) {
                dishClickListener?.onAddBtnClick(dish, position)
            }
        }

        holder.itemView.tv_add_dish.setOnClickListener {
            if (position != RecyclerView.NO_POSITION) {
                dishClickListener?.onAddDishClick(dish, position)
            }
        }

        holder.itemView.tv_remove_dish.setOnClickListener {
            if (position != RecyclerView.NO_POSITION) {
                dishClickListener?.onRemoveDishClick(dish, position)
            }
        }
    }


    class DishViewHolder(item: View) : RecyclerView.ViewHolder(item) {
    }

    interface OnDishClickListener {
        fun onAddDishClick(dish: Dish, position: Int)
        fun onRemoveDishClick(dish: Dish, position: Int)
        fun onAddBtnClick(dish: Dish, position: Int)
    }

    fun setOnDishClickListener(listener: OnDishClickListener) {
        this.dishClickListener = listener
    }


}