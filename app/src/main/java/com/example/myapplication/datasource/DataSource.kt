package com.example.myapplication.datasource

import com.example.myapplication.model.Dish

class DataSource {

    companion object {
        fun getListOfDish(): ArrayList<Dish> {

            val dishList = ArrayList<Dish>()

            dishList.add(Dish(1, "Masala Dosa", 0, 50, "This is Masala Dosa"))
            dishList.add(Dish(2, "Ragi Dosa", 0, 70, "This is Ragi Dosa"))
            dishList.add(Dish(3, "Onion Dosa", 0, 90, "This is Onion Dosa"))
            dishList.add(Dish(4, "Paneer Dosa", 0, 150, "This is Paneer Dosa"))
            dishList.add(Dish(5, "Set Dosa", 0, 80, "This is Set Dosa"))
            dishList.add(Dish(6, "Spunj Dosa", 0, 90, "This is Spunj Dosa"))
            dishList.add(Dish(7, "Plain Dosa", 0, 50, "This is Plain Dosa"))
            dishList.add(Dish(8, "Idli Vada", 0, 130, "This is Idli Vada"))
            dishList.add(Dish(9, "Parotha", 0, 140, "This is Parotha"))
            dishList.add(Dish(10, "Veg Pulav", 0, 150, "This is Veg Pulav"))

            return dishList

        }
    }


}