package com.example.myapplication.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.R
import com.example.myapplication.adapter.DishListAdapter
import com.example.myapplication.datasource.DataSource
import com.example.myapplication.model.Dish
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    val dishList = ArrayList<Dish>()
    val adapter = DishListAdapter()
    val LOG_TAG = MainActivity::class.java.simpleName
    var totalDish = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.title = getString(R.string.menu_screen_title)

        tv_view_cart?.setOnClickListener(this)

        //get list of Dish
        dishList.addAll(DataSource.getListOfDish())

        rv_dish.adapter = adapter
        rv_dish.setHasFixedSize(true)
        adapter.submitList(dishList)


        // After add dish in cart
        adapter.setOnDishClickListener(object : DishListAdapter.OnDishClickListener {

            override fun onRemoveDishClick(dish: Dish, position: Int) {
                dishList[position].count -= 1
                adapter.notifyItemChanged(position)
                calculateViewCart()
            }

            override fun onAddBtnClick(dish: Dish, position: Int) {
                dishList[position].count = 1
                adapter.notifyItemChanged(position)

                calculateViewCart()
            }

            override fun onAddDishClick(dish: Dish, position: Int) {

                if (dishList[position].count == 20) {
                    Toast.makeText(this@MainActivity, getString(R.string.cannot_add_more_than_20_items), Toast.LENGTH_LONG)
                        .show()
                } else {
                    dishList[position].count += 1
                }

                adapter.notifyItemChanged(position)

                calculateViewCart()
            }
        })


    }

    private fun calculateViewCart() {

        totalDish = 0

        for (dish in dishList) {
            totalDish += dish.count
        }

        if (totalDish != 0) {
            cl_view_cart.visibility = View.VISIBLE
            tv_view_cart?.text = String.format(getString(R.string.view_cart_items, totalDish))
        } else {
            cl_view_cart.visibility = View.GONE
        }
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.tv_view_cart -> {
                val intent = Intent(this, CartActivity::class.java)
                intent.putParcelableArrayListExtra("viewCartList", getViewCartList())
                startActivity(intent)
            }
        }
    }

    private fun getViewCartList(): ArrayList<Dish>? {

        val viewCartList = ArrayList<Dish>()

        for (dish in dishList) {
            if (dish.count > 0) {
                viewCartList.add(dish)
            }
        }

        return viewCartList
    }
}
