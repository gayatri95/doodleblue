package com.example.myapplication.view

import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.R
import com.example.myapplication.adapter.ViewCartAdapter
import com.example.myapplication.model.Dish
import kotlinx.android.synthetic.main.activity_cart.*

class CartActivity : AppCompatActivity() {

    val LOG_TAG = CartActivity::class.java.simpleName
    val adapter = ViewCartAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)

        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.title = getString(R.string.view_cart_title)

        val extras = intent.extras
        val dishList = extras?.getParcelableArrayList<Parcelable>("viewCartList") as ArrayList<Dish>

        Log.e(LOG_TAG, dishList.toString())

        rv_dish.adapter = adapter
        adapter.submitList(dishList)


    }
}
